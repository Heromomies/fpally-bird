﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    public float maxTime = 1;
    public GameObject pipe;
    public float height;
    public float timeDecrease;
    public float minTime;
    
    private float _timer = 0;

    // Update is called once per frame
    void Update()
    {
        if (_timer > maxTime)
        {
            GameObject newPipe = Instantiate(pipe);
            newPipe.transform.position = transform.position + new Vector3(0, Random.Range(-height, height), 0);
            Destroy(newPipe, 15);
            _timer = 0;
            if (maxTime > minTime) {
                maxTime -= timeDecrease;
            }
            if (maxTime <= 1.3)
            {
                maxTime = 1.3f;
            }
        }

        _timer += Time.deltaTime;
    }
}
